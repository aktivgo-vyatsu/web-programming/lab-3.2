let posX = 0;
let posY = 0;
let x = 0;
let y = 0;

onmousemove = (e) => {
    x = e.pageX;
    y = e.pageY;
}

setInterval(() => {
    posX = Math.floor((49 * posX + x) / 50);
    posY = Math.floor((49 * posY + y) / 50);
    let circle = document.getElementById('circle');
    circle.style.left = posX + 'px';
    circle.style.top = posY + 'px';
}, 5);