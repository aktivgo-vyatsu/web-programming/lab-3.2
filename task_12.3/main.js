let posX1 = 0;
let posY1 = 0;
let posX2 = 0;
let posY2 = 0;
let x = 0;
let y = 0;

onmousemove = (e) => {
    x = e.pageX;
    y = e.pageY;
}

setInterval(() => {
    posX1 = Math.floor((49 * posX1 + x)/50);
    posY1 = Math.floor((49 * posY1 + y)/50);
    let circle = document.getElementById('circle_1');
    circle.style.left = (posX1 - 60) +'px';
    circle.style.top = posY1 +'px';
}, 5);

setInterval(() => {
    posX2 = Math.floor((49 * posX2 + x)/50);
    posY2 = Math.floor((49 * posY2 + y)/50);
    let circle = document.getElementById('circle_2');
    circle.style.left = (posX2 + 60) +'px';
    circle.style.top = posY2 +'px';
}, 10);