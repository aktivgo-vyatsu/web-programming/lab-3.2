document.getElementById('datetime').onchange = () => {
    const currDate = new Date();

    let datetime = document.getElementById('datetime').value;

    const chosenDate = new Date(datetime);
    if (chosenDate < currDate) {
        document.getElementById("result").value = 'Эта дата уже прошла';
        return;
    }

    const diff = new Date(chosenDate - new Date());
    let strDifference = '';
    if (diff.getFullYear() > 1970) {
        strDifference += `${diff.getFullYear() - 1970} лет `;
    }
    if (diff.getMonth() > 0) {
        strDifference += `${diff.getMonth()} месяцев `;
    }
    if (diff.getDate() > 1) {
        strDifference += `${diff.getDate() - 1} дней `;
    }
    const offset = Math.abs(diff.getTimezoneOffset()) / 60;
    if (diff.getHours() > offset) {
        strDifference += `${diff.getHours() - offset} часов `;
    }
    if (diff.getMinutes() > 0) {
        strDifference += `${diff.getMinutes()} минут`;
    }
    if (strDifference.length === 0) {
        strDifference = 'Это сейчас';
    }
    document.getElementById('result').value = strDifference;
}