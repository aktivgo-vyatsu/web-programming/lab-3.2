let startTime;

onload = () => {
    startTime = new Date();
}

onbeforeunload = () => {
    document.getElementById('totalTime').innerText = `total time ${(new Date() - startTime)/1000} s`;
}