onload = () => {
    let seconds = new Date().getSeconds();
    document.body.style.backgroundColor = `rgb(${seconds * 10 % 255}, ${seconds * 100 % 255}, ${seconds * 1000 % 255})`;
}